﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine.EventSystems;

public class ZNotificationMgr : ZSingleton<ZNotificationMgr> 
{
	/// <summary>
	/// Inexact uses `set` method
	/// Exact uses `setExact` method
	/// ExactAndAllowWhileIdle uses `setAndAllowWhileIdle` method
	/// Documentation: https://developer.android.com/intl/ru/reference/android/app/AlarmManager.html
	/// </summary>
	public enum NotificationExecuteMode
	{
		Inexact = 0,
		Exact = 1,
		ExactAndAllowWhileIdle = 2
	}

	#if UNITY_ANDROID && !UNITY_EDITOR
	private static string fullClassName = "net.agasper.unitynotification.UnityNotificationManager";
	private static string mainActivityClassName = "com.unity3d.player.UnityPlayerActivity";
	#endif

	public void AddNotification(string text, int addMinutes)
	{
		#if UNITY_IOS
		ScheduleNotificationForiOSWithMessage (text, DateTime.Now.AddMinutes(addMinutes));
		#elif UNITY_ANDROID
		ScheduleNotificationForAndroidMessage (1, addMinutes, text, text, new Color32(0xff, 0x44, 0x44, 255));
		#endif
	}

	public void ClearNotifications()
	{
		#if UNITY_IOS
		if (Application.platform == RuntimePlatform.IPhonePlayer) 
		{
		UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications ();
		}
		#elif UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass pluginClass = new AndroidJavaClass(fullClassName);
		if (pluginClass != null) 
		{
		pluginClass.CallStatic("CancelNotification", 1);
		}
		#endif
	}

	public static void ScheduleNotificationForAndroidMessage(int id, long delay, string title, string message, Color32 bgColor, bool sound = true, bool vibrate = true, bool lights = true, string bigIcon = "", NotificationExecuteMode executeMode = NotificationExecuteMode.Inexact)
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass pluginClass = new AndroidJavaClass(fullClassName);
		if (pluginClass != null)
		{
		pluginClass.CallStatic("SetNotification", id, delay * 1000L, title, message, message, sound ? 1 : 0, vibrate ? 1 : 0, lights ? 1 : 0, bigIcon, "notify_icon_small", bgColor.r * 65536 + bgColor.g * 256 + bgColor.b, (int)executeMode, mainActivityClassName);
		}
		#endif
	}

	void ScheduleNotificationForiOSWithMessage (string text, System.DateTime fireDate)
	{
		#if UNITY_IOS
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
		UnityEngine.iOS.LocalNotification notification = new UnityEngine.iOS.LocalNotification ();
		notification.fireDate = fireDate;
		notification.alertAction = "Alert";
		notification.alertBody = text;
		notification.hasAction = false;

		UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(notification);

		UnityEngine.iOS.NotificationServices.RegisterForNotifications (UnityEngine.iOS.NotificationType.Alert | UnityEngine.iOS.NotificationType.Badge);
		}        
		#endif
	}

	/*public void RegisterNotification()
	{
	}*/

	public void StartNotification()
	{/*
		UnityEngine.iOS.LocalNotification n = new UnityEngine.iOS.LocalNotification();
		n.fireDate = DateTime.Now.AddSeconds(10);
		n.alertAction = "Title Here";
		n.alertBody = "My Alert Body Text";
		n.applicationIconBadgeNumber = 99;
		n.hasAction = true;//?
		n.repeatCalendar = UnityEngine.iOS.CalendarIdentifier.GregorianCalendar;
		n.repeatInterval = UnityEngine.iOS.CalendarUnit.Day;
		n.soundName = UnityEngine.iOS.LocalNotification.defaultSoundName;
		UnityEngine.iOS.NotificationServices.ScheduleLocalNotification (n);*/
	}
}



