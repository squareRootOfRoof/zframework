﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ZPachingkoManager : ZSingleton<ZPachingkoManager> 
{
//	public static ZPachingkoManager Instance;
//	public static ZPachingkoManager Instance()
//	{
//		if (s_instance == null) 
//		{
//			GameObject 
//		}
//		return s_instance;
//	}

	public List<ZShopItem> m_listOfCommonItems 		= new List<ZShopItem>();
	public List<ZShopItem> m_listOfUncommonItems	= new List<ZShopItem>(); 
	public List<ZShopItem> m_listOfRareItems 		= new List<ZShopItem> ();
	public List<ZShopItem> m_listOfQueuedItems 		= new List<ZShopItem>();

	//public Dictionary<string, ZShopItem> m_listOfItems = new Dictionary<string, ZShopItem> ();

	// + EDIT JOSH 
	// Make the total values sum of to a total of 100
	public float m_commonPercentage	 				= 55.0f;
	public float m_uncommonPercentage				= 30.0f;
	public float m_rarePercentage					= 15.0f;

	private int m_queueIterator;

	public void Awake()
	{
		//s_instance = this;
	}

	public void ClearList()
	{
		m_listOfCommonItems.Clear();
		m_listOfUncommonItems.Clear();
		m_listOfRareItems.Clear ();
		m_listOfQueuedItems.Clear ();
	}

	public void RegisterItem(ZShopItem p_itemData)
	{
		switch (p_itemData.ItemRarity) 
		{
			case "Common":
			{
				m_listOfCommonItems.Add (p_itemData);
				break;
			}
			case "Uncommon":
			{
				m_listOfUncommonItems.Add (p_itemData);
				break;
			}
			case "Rare":
			{
				m_listOfRareItems.Add (p_itemData);
				break;
			}
		}
	}

	public ZShopItem GetRandomCommon()
	{
		int randomVal = Random.Range (0, m_listOfCommonItems.Count);
		ZShopItem retVal = m_listOfCommonItems [randomVal];

		m_listOfCommonItems.Remove (retVal);
		return retVal;
	}

	public ZShopItem GetRandomUncommon()
	{
		int randomVal = Random.Range (0, m_listOfUncommonItems.Count);
		ZShopItem retval = m_listOfUncommonItems [randomVal];

		m_listOfUncommonItems.Remove (retval);
		return retval;
	}

	public ZShopItem GetRandomRare()
	{
		int randomVal = Random.Range (0, m_listOfRareItems.Count);
		ZShopItem retVal = m_listOfRareItems [randomVal];

		m_listOfRareItems.Remove (retVal);
		return retVal;
	}

	public ZShopItem GetRandomAll()
	{
		int randomVal = Random.Range (0, 100);
		ZShopItem retVal = new ZShopItem();

		float uncommonPercentage = (m_commonPercentage + m_uncommonPercentage);
		float rarePercentage = m_commonPercentage + m_uncommonPercentage + m_rarePercentage;

		if (randomVal > m_commonPercentage) 
		{
			retVal = GetRandomCommon ();
		}
		else if(randomVal >= m_commonPercentage && randomVal <= uncommonPercentage)
		{
			retVal = GetRandomUncommon ();
		}
		else if(rarePercentage > randomVal)
		{
			retVal = GetRandomRare ();
		}

		return retVal;
	}

	public ZShopItem GetQueuedItem()
	{
		ZShopItem retVal = m_listOfQueuedItems [m_queueIterator];

		m_listOfCommonItems.Remove (retVal);
		m_listOfUncommonItems.Remove (retVal);
		m_listOfRareItems.Remove (retVal);

		return retVal;
	}

}
