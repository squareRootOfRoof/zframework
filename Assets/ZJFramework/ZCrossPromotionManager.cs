﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartboostSDK;
using UnityEngine.UI;
using Tapdaq;

public class ZCrossPromotionManager : ZSingleton<ZCrossPromotionManager> 
{
    [Space]
    [SerializeField]
    CanvasGroup m_CanvasGroup;

    [SerializeField]
    GameObject m_CrossPromoPanel;

    [SerializeField]
    Image m_ImageComponent;

    bool m_canInteract;
    TDNativeAd m_TDNativeAd;

    private void Start()
    {
        AdManager.Init();
    }

    #region For Tapdaq Delegates
    private void OnEnable()
    {
        TDCallbacks.TapdaqConfigLoaded += OnTapdaqConfigLoaded;
        TDCallbacks.AdAvailable += OnAdAvailable;
    }

    private void OnDisable()
    {
        TDCallbacks.TapdaqConfigLoaded -= OnTapdaqConfigLoaded;
        TDCallbacks.AdAvailable -= OnAdAvailable;
    }

    private void OnTapdaqConfigLoaded()
    {
        AdManager.LoadNativeAdvertForTag("ingame", TDNativeAdType.TDNativeAdType2x3Large);
    }

    private void OnAdAvailable(TDAdEvent e)
    {
        if (e.adType == "NATIVE_AD" && e.tag == "ingame")
            m_TDNativeAd = AdManager.GetNativeAd(TDNativeAdType.TDNativeAdType2x3Large, "ingame");
    }
    #endregion

    public void forceShowCrossPromo()
	{
        #if UNITY_EDITOR
        StartCoroutine(CrossPromotionSequence(true));
        return;
        #endif

        if (m_TDNativeAd == null)
            return;

        m_TDNativeAd.LoadTexture((TDNativeAd obj) =>
        {
            //Callback if texture is loaded!
            Texture2D _NewTexture = obj.texture;
            if (_NewTexture == null)
            {
                Debug.LogError("Texture not loaded");
                return;
            }

            Sprite _GetSprite = Sprite.Create(_NewTexture, new Rect(0, 0, _NewTexture.width, _NewTexture.height), new Vector2(0.5f, 0.5f), 100.0f);
            m_ImageComponent.sprite = _GetSprite;
            AdManager.SendNativeImpression(m_TDNativeAd);
            StartCoroutine(CrossPromotionSequence(true));
        });
	}

    public void CrossPromoButtonCallback(bool _ToOpen)
    {
        if (!m_canInteract)
            return;

        if (_ToOpen)
        {
            AdManager.SendNativeClick(m_TDNativeAd);
            Unpause();
        }
        else
        {
            StartCoroutine(CrossPromotionSequence(false));
        }
    }

    IEnumerator CrossPromotionSequence(bool _ToShow)
    {
        m_canInteract = false;

        if (_ToShow)
        {
            //Pause!
            //Time.timeScale = 0;

            m_CrossPromoPanel.SetActive(true);
            m_CanvasGroup.alpha = 0;
        }

        float _ToAlpha = _ToShow ? 1 : 0;

        while (m_CanvasGroup.alpha != _ToAlpha)
        {
            yield return new WaitForEndOfFrame();
            m_CanvasGroup.alpha = Mathf.MoveTowards(m_CanvasGroup.alpha, _ToAlpha, Time.deltaTime * 2);
        }

        if (!_ToShow)
            Unpause();

        m_canInteract = true;
    }

    void Unpause()
    {
        m_CrossPromoPanel.SetActive(false);
        //Time.timeScale = 1;
    }
}
