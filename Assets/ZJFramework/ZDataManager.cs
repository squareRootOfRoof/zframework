﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.Security.Cryptography;
using System.Diagnostics;
using System.IO;
using System.Text;
using System;

// Use player prefs but with serialization and encrypting

public static class ZDataManager 
{
	public static string Serialize<T>(this T toSerialize)
	{
		XmlSerializer xml = new XmlSerializer(typeof(T));
		StringWriter writer = new StringWriter();
		xml.Serialize(writer, toSerialize);
		return writer.ToString();
	}

	public static T Deserialize<T>(this string toDesrialize)
	{
		XmlSerializer xml = new XmlSerializer(typeof(T));
		StringReader reader = new StringReader(toDesrialize);
		return (T)xml.Deserialize(reader);
	}

	private static string hash = "qwer1234";

	public static void Save(string p_paramName, string p_value)
	{
		PlayerPrefs.SetString (p_paramName, Encrypt(p_value));
	}

	public static void Save(string p_paramName, int p_value)
	{
		PlayerPrefs.SetString (p_paramName, Encrypt("" + p_value));
	}

	public static void Save (string p_paramName, float p_value)
	{
		PlayerPrefs.SetString(p_paramName, Encrypt("" + p_value));
	}

	public static string LoadString (string p_paramName)
	{
		string data = PlayerPrefs.GetString (p_paramName);
		return Decrypt (data);
	}

	public static float LoadFloat(string p_paramName)
	{
		string data = PlayerPrefs.GetString (p_paramName);
		return float.Parse(Decrypt (data));
	}

	public static int LoadInt(string p_paramName)
	{
		string data = PlayerPrefs.GetString (p_paramName);
		return int.Parse (Decrypt (data));
	}

	public static string Encrypt(string input)
	{
		byte[] data = UTF8Encoding.UTF8.GetBytes (input);
		using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider ()) 
		{
			byte[] key = md5.ComputeHash (UTF8Encoding.UTF8.GetBytes (hash));
			using (TripleDESCryptoServiceProvider trip = new TripleDESCryptoServiceProvider () {
																							Key = key,
																							Mode = CipherMode.ECB,
																						Padding = PaddingMode.PKCS7																	}) 
			{
				ICryptoTransform tr = trip.CreateEncryptor ();
				byte[] results = tr.TransformFinalBlock (data, 0, data.Length);
				return Convert.ToBase64String (results, 0, results.Length);
			}
		}
	}

	public static string AesEncrypt(string p_input)
	{
		byte[] data = UTF8Encoding.UTF8.GetBytes (p_input);

		using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider ()) {
			byte[] key = md5.ComputeHash (UTF8Encoding.UTF8.GetBytes (hash));
			using (AesCryptoServiceProvider aes = new AesCryptoServiceProvider () {
				Key = key,
				Mode = CipherMode.ECB,
				Padding = PaddingMode.PKCS7
			}) {
				ICryptoTransform tr = aes.CreateEncryptor ();
				byte[] results = tr.TransformFinalBlock (data, 0, data.Length);
				return Convert.ToBase64String (results, 0, results.Length);
			}
		}
	}

	public static string Decrypt(string input)
	{
		byte[] data = Convert.FromBase64String (input);
		using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider ()) 
		{
			byte[] key = md5.ComputeHash (UTF8Encoding.UTF8.GetBytes (hash));
			using (TripleDESCryptoServiceProvider trip = new TripleDESCryptoServiceProvider () {
				Key = key,
				Mode = CipherMode.ECB,
				Padding = PaddingMode.PKCS7
			}) 
			{
				ICryptoTransform tr = trip.CreateDecryptor ();
				byte[] results = tr.TransformFinalBlock (data, 0, data.Length);
				return UTF8Encoding.UTF8.GetString (results);
			}
		}
	}
}
