﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class ZShopItem 
{
	public int ItemID;
	public string ItemName;
	public Sprite ItemSprite;
	public GameObject ItemObject;
	public string ItemRarity;
	public float InGamePrice;
	public float IAPPrice;
	public string AndroidItemId;
	public string IOSItemId;
	public bool IsPurchased;
}

[ExecuteInEditMode]
public class ZShopEditor: MonoBehaviour
{
	private static List<Dictionary<string, object>> m_dataDictList = new List<Dictionary<string, object>> ();


	private static void ReloadShopItems()
	{
		ClearData();
		LoadData ();
		InitializeItemScreen ();
	}

    #if UNITY_EDITOR
	[MenuItem("ZFramework/Setup Shop")]
	static void RegisterShop() 
	{
		Debug.Log ("Reloading Shop Items");
		ReloadShopItems ();
	}
    #endif

	private static void ClearData()
	{
		foreach (ZShopItemEntry entry in ZShopManager.Instance.m_listOfItemsEntry) 
		{
			DestroyImmediate (entry.gameObject);
		}

		ZShopManager.Instance.m_listOfItems.Clear ();
		ZShopManager.Instance.m_listOfItemsEntry.Clear ();
		ZPachingkoManager.Instance.ClearList ();

		RectTransform contextRect = ZShopManager.Instance.m_objectContentView.GetComponent<RectTransform> ();
		contextRect.sizeDelta = new Vector2 (10.0f, contextRect.sizeDelta.y);
	}

	private static void LoadData()
	{
		m_dataDictList = CSVReader.Read ("ShopData");

		for (int idx = 0; idx < m_dataDictList.Count; idx++) 
		{
			string itemIndex = "" + m_dataDictList [idx] ["Index"];
			string itemName = "" + m_dataDictList[idx]["ItemName"];
			string itemInGamePrice = "" + m_dataDictList[idx]["InGame_Price"];
			string itemIAPPrice = "" + m_dataDictList [idx] ["IAP_Price"];
			string itemImage = "" + m_dataDictList [idx] ["Image"];
			string itemRarity = "" + m_dataDictList [idx] ["Rarity"];
			string itemObject = "" + m_dataDictList [idx] ["Object"];
			string androidItemId = "" + m_dataDictList [idx]["Android_ItemId"];
			string iosItemId = "" + m_dataDictList [idx] ["iOS_ItemId"];

			ZShopItem shopItem = new ZShopItem ();
			shopItem.ItemID = int.Parse(itemIndex);
			shopItem.ItemName = itemName;
			shopItem.InGamePrice = float.Parse(itemInGamePrice);
			shopItem.IAPPrice = float.Parse(itemIAPPrice);
			shopItem.ItemRarity = itemRarity;
			shopItem.ItemSprite = Resources.Load<Sprite>(itemImage) as Sprite;
			shopItem.ItemObject = Resources.Load<GameObject>(itemObject) as GameObject;
			shopItem.IsPurchased = false;
			shopItem.AndroidItemId = androidItemId;
			shopItem.IOSItemId = iosItemId;
			shopItem.IsPurchased = (PlayerPrefs.GetInt ("Item" + idx) == 1) ? true : false;


			RegisterItem (shopItem);
		}
	}

	private static void InitializeItemScreen()
	{
		GameObject objectPrefab = ZShopManager.Instance.m_objectPrefab;
		GameObject objectContentView = ZShopManager.Instance.m_objectContentView;


		for(int idx = 0; idx < ZShopManager.Instance.m_listOfItems.Count; idx++)
		{
			GameObject objInstance = Instantiate (objectPrefab, new Vector3(0, 0, 0), Quaternion.identity);
			objInstance.transform.parent = objectContentView.transform;
			objInstance.transform.localPosition = new Vector3 (100, 100, 100);
			objInstance.transform.localScale = new Vector3 (0.7f, 0.7f, 0.7f);
			objInstance.GetComponent<RectTransform> ().anchoredPosition3D = new Vector3 (0, 0, 0);

			ZShopManager.Instance.m_listOfItemsEntry.Add (objInstance.GetComponent<ZShopItemEntry>());
			RectTransform contextRect = objectContentView.GetComponent<RectTransform> ();
			contextRect.sizeDelta = new Vector2 (contextRect.sizeDelta.x + 90.0f, contextRect.sizeDelta.y);
		}
	}
		
	// Register all items in the pachingko
	private static void RegisterItem(ZShopItem p_item)
	{
		ZShopManager.Instance.m_listOfItems.Add(p_item);
		ZPachingkoManager.Instance.RegisterItem (p_item);
	}
}
	
public class ZShopManager: ZSingleton<ZShopManager>
{
	public static float MIN_DISTANCE = 0.1f;
	public static Vector2 CENTER_VEC = Vector2.zero;

	public List<ZShopItem> m_listOfItems	= new List<ZShopItem> ();
	public List<ZShopItemEntry> m_listOfItemsEntry = new List<ZShopItemEntry>();

	public GameObject m_objectContentView;
	public GameObject m_objectPrefab;
	public GameObject m_shopScreen;

	public Text m_textButton;
	public int m_itemHighlighted;
	public int m_selectedItem;

	public delegate void OnBuyitem(ZShopItem p_item);
	public event OnBuyitem onBuyitem;

	private bool m_onTouch;


	public void Awake()
	{
		RefreshScreen ();
	}

	public void Update()
	{
		if (Input.GetMouseButton (0)) 
		{
			m_onTouch = true;
		} 
		else if (Input.GetMouseButtonUp (0)) 
		{
			m_onTouch = false;
		}

		float xValue = m_itemHighlighted * 100.0f;

		Vector3 targetPos = new Vector3 (-xValue, 0.0f, 0.0f);

		if (!m_onTouch) {
	
			Vector3 anchorPos = m_objectContentView.GetComponent<RectTransform> ().anchoredPosition3D;
			float dist = Vector3.Distance (anchorPos, targetPos);

			if (dist > 1.0f) {
				m_objectContentView.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.MoveTowards (anchorPos, targetPos, 10.0f);
			}
		} 
		else 
		{
			for (int idx = 0; idx < m_listOfItemsEntry.Count; idx++) {
				float dist = Vector3.Distance (m_listOfItemsEntry [idx].transform.position, new Vector3 (0, 4.7f, 0));

				if (dist < 91) {
					if (m_itemHighlighted != idx) {
						m_listOfItemsEntry [m_itemHighlighted].transform.localScale = new Vector3 (0.7f, 0.7f, 0.7f);
					}
				
					m_itemHighlighted = idx;

					Vector3 targetVec = new Vector3 (1, 1, 1);
					m_listOfItemsEntry [idx].transform.localScale = targetVec;

					m_textButton.text = (!m_listOfItems [idx].IsPurchased) ? ("" + m_listOfItems [idx].InGamePrice) : ("SELECT");
				}
			}
		}
	}

	public void ClearData()
	{
		foreach(ZShopItemEntry entry in m_listOfItemsEntry)
		{
			DestroyImmediate (entry.gameObject);
		}

		m_listOfItems.Clear ();
		m_listOfItemsEntry.Clear ();

		RectTransform contextRect = m_objectContentView.GetComponent<RectTransform> ();
		contextRect.sizeDelta = new Vector2 (250.0f, contextRect.sizeDelta.y);

		ZPachingkoManager.Instance.ClearList ();
	}

	public void Buy()
	{
		onBuyitem (m_listOfItems [m_itemHighlighted]);
	}

	public List<ZShopItem> GetAllItems()
	{
		return m_listOfItems;
	}

	public ZShopItem GetItem(int p_id)
	{
		return m_listOfItems [p_id];
	}

	public void ConfirmPurchase(int p_id)
	{
		m_listOfItems [p_id].IsPurchased = true;
		PlayerPrefs.SetInt ("SelectedItem", p_id);
		PlayerPrefs.SetInt ("Item" + p_id, 1);
	}

	public void ResetItems()
	{
		for (int idx = 0; idx < m_listOfItems.Count; idx++) 
		{
			PlayerPrefs.DeleteKey ("Item" + idx);
		}
	}



	private void RefreshScreen()
	{
		m_selectedItem = PlayerPrefs.GetInt ("SelectedItem");
		m_itemHighlighted = PlayerPrefs.GetInt ("SelectedItem");

		float xValue = m_selectedItem * 100.0f;

		Vector3 targetPos = new Vector3 (-xValue, 0.0f, 0.0f);
		m_objectContentView.GetComponent<RectTransform> ().anchoredPosition3D = targetPos;
		Vector3 test = m_objectContentView.GetComponent<RectTransform> ().anchoredPosition3D;

		for(int idx = 0; idx < m_listOfItemsEntry.Count; idx++) 
		{
			m_listOfItemsEntry[idx].Initialize (m_listOfItems[idx]);
		}
	}






}
