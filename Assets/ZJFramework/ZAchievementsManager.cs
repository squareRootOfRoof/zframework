﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class ZAchievementsData
{
	public string AchievementTitle;
	public int AchievementID;
	public Sprite IconAchievementLocked;
	public Sprite IconAchievementUnlocked;
	public bool IsCompleted;
	public bool IsClaimed;
	public string AchievementDescription;
	public float ProgressToComplete;
	public float CurrentProgress;
	public float Reward;

	public void SetProgress(float p_achivementProgress)
	{
		CurrentProgress += p_achivementProgress;
		if (CurrentProgress >= ProgressToComplete) 
		{
			PlayerPrefs.SetInt ("Achievement_Completed" + AchievementID, 1);
			IsCompleted = true;
		} 
		else 
		{
			PlayerPrefs.SetFloat ("Achievement_Progress" + AchievementID, CurrentProgress);
		}
	}
}

[ExecuteInEditMode]
public class ZAchievementEditor : MonoBehaviour
{
	private static Vector3 DEFAULT_OBJ_SCALE = new Vector3(1.0f, 1.0f, 1.0f);
	private static float Y_INCREMENT_VALUE   = 40.0f;

	private static List<Dictionary<string, object>> m_dataDictList = new List<Dictionary<string, object>>();

    #if UNITY_EDITOR
	[MenuItem("ZFramework/Setup Achievements")]
	static void RegisterShop() 
	{
		Debug.Log ("Reloading Achievements");
		ReloadAchievements ();
		Debug.Log ("Achievements properly loaded");
	}
    #endif

	private static void ReloadAchievements()
	{
		ClearData();
		LoadData ();
		InitializeAchivementScreen ();
	}

	private static void ClearData()
	{
		foreach (ZAchievementEntry entry in ZAchievementsManager.Instance.m_listOfAchievementsEntry) 
		{
			DestroyImmediate (entry.gameObject);
		}

		ZAchievementsManager.Instance.m_listOfAchievements.Clear ();
		ZAchievementsManager.Instance.m_listOfAchievementsEntry.Clear ();

		RectTransform contextRect = ZAchievementsManager.Instance.m_objectContentView.GetComponent<RectTransform> ();
		contextRect.sizeDelta = new Vector2 (450.0f, contextRect.sizeDelta.y);
	}

	// + EDIT JOSH 
	// load data from the csv files under the resources folder
	private static void LoadData()
	{
		m_dataDictList = CSVReader.Read ("AchievementsData");

		for (int idx = 0; idx < m_dataDictList.Count; idx++) 
		{
			string achievementTitle 		= "" + m_dataDictList [idx] ["AchievementTitle"];
			string achievementDesc 			= "" + m_dataDictList[idx]["AchievementDesc"];
			string achievementID 			= "" + m_dataDictList[idx]["AchievementID"];
			string achievementIconUnlocked 	= "" + m_dataDictList [idx] ["IconAchievementUnlocked"];
			string achievementIconLocked 	= "" + m_dataDictList [idx] ["IconAchievementLocked"];
			string isAchievementComplete 	= "" + m_dataDictList [idx] ["IsComplete"];
			string progressToComplete 		= "" + m_dataDictList [idx]["ProgressToComplete"];
			string achievementReward 		= "" + m_dataDictList [idx] ["Reward"];

			ZAchievementsData data = new ZAchievementsData();

			data.AchievementTitle	 		= achievementTitle;
			data.AchievementDescription 	= achievementDesc;
			data.AchievementID 				= idx;
			data.IconAchievementLocked 		= Resources.Load<Sprite>(achievementIconLocked) as Sprite;
			data.IconAchievementUnlocked 	= Resources.Load<Sprite>(achievementIconUnlocked) as Sprite;
			data.IsCompleted 				= (PlayerPrefs.GetInt("Achievement_Completed" + idx) == 1) ? true : false;
			data.ProgressToComplete 		= float.Parse (progressToComplete);
			data.CurrentProgress 			= PlayerPrefs.GetFloat ("Achievement_Progress" + idx);
			data.Reward 					= float.Parse (achievementReward);
			data.IsClaimed 					= (PlayerPrefs.GetInt ("Achievement_Claimed" + idx) == 1) ? true : false;

			ZAchievementsManager.Instance.m_listOfAchievements.Add (data);
		}
	}

	// Initialize Achivement Screen
	private static void InitializeAchivementScreen()
	{
		GameObject objectContentView 	= ZAchievementsManager.Instance.m_objectContentView;
		GameObject objectPrefab 		= ZAchievementsManager.Instance.m_objectPrefab;

		for (int idx = 0; idx < ZAchievementsManager.Instance.m_listOfAchievements.Count; idx++) 
		{
			ZAchievementsData data = ZAchievementsManager.Instance.m_listOfAchievements [idx];

			GameObject objInstance = Instantiate (objectPrefab, Vector3.zero, Quaternion.identity);
			objInstance.transform.parent = objectContentView.transform;
			objInstance.transform.localScale = DEFAULT_OBJ_SCALE;
			objInstance.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;

			Sprite achievementIcon = (data.IsCompleted) ? data.IconAchievementUnlocked : data.IconAchievementLocked;

			ZAchievementsManager.Instance.m_listOfAchievementsEntry.Add (objInstance.GetComponent<ZAchievementEntry>());

			RectTransform contextRect = objectContentView.GetComponent<RectTransform> ();
			contextRect.sizeDelta = new Vector2 (contextRect.sizeDelta.x, contextRect.sizeDelta.y + Y_INCREMENT_VALUE);
		}
	}
}
	
public class ZAchievementsManager : ZSingleton<ZAchievementsManager> 
{
	public List<ZAchievementsData> m_listOfAchievements = new List<ZAchievementsData>();
	public List<ZAchievementEntry> m_listOfAchievementsEntry = new List<ZAchievementEntry>();

	public delegate void OnAchievementClaimed(int p_id);
	public event OnAchievementClaimed onAchievementClaimed;

	private static Vector2 DEFAULT_CONTEXT_SIZE = new Vector2 (0.0f, 450.0f);

	public static ZAchievementsManager s_instance;

	public GameObject m_objectContentView;
	public ZAchievementBar m_objectAchievementBar;
	public GameObject m_objectPrefab;

	void Awake () 
	{
		RefreshScreen ();
	}
		
//	public void ClearData()
//	{
//		foreach (ZAchievementEntry entry in m_listOfAchievementsEntry) 
//		{
//			DestroyImmediate (entry.gameObject);
//		}
//
//		RectTransform contextRect = m_objectContentView.GetComponent<RectTransform> ();
//		contextRect.sizeDelta = new Vector2 (450.0f, contextRect.sizeDelta.y);
//
//		m_listOfAchievements.Clear ();
//		m_listOfAchievementsEntry.Clear ();
//	}

	// Retrieve achivement data
	public ZAchievementsData GetAchievement(int p_achievementID)
	{
		return m_listOfAchievements [p_achievementID];
	}

	// Set Progress for an achievement
	public void SetProgress(int p_achievmentID, float p_achievmentProgress)
	{
		m_listOfAchievements [p_achievmentID].SetProgress (p_achievmentProgress);

		if (m_listOfAchievements [p_achievmentID].IsCompleted) 
		{
			m_objectAchievementBar.DisplayAchievement (m_listOfAchievements [p_achievmentID]);
		}

		RefreshScreen ();
	}

	public void ResetAchievements()
	{
		for (int idx = 0; idx < m_listOfAchievements.Count; idx++) 
		{
			PlayerPrefs.DeleteKey ("Achievement_Claimed" + idx);
			PlayerPrefs.DeleteKey ("Achievement_Completed" + idx);
			PlayerPrefs.DeleteKey ("Achievement_Progress" + idx);
		}
	}

	public void ClaimButton(int p_id)
	{
		PlayerPrefs.SetInt ("Achievement_Claimed" + p_id, 1);
		onAchievementClaimed (p_id);
	}

	public void RefreshScreen()
	{
		m_objectContentView.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
		for(int idx = 0; idx < m_listOfAchievementsEntry.Count; idx++) 
		{
			m_listOfAchievementsEntry[idx].Initialize (m_listOfAchievements[idx]);
		}
	}
}
