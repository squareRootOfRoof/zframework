﻿/// <summary>
/// Z Achievement Entry.
/// </summary>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZAchievementEntry : MonoBehaviour 
{
	public int m_achievementID				= 0;

	public Text m_textAchievementTitle		= null;
	public Text m_textAchievementDesc		= null;
	public Text m_textProgress				= null;

	public GameObject m_objClaimBtn			= null;
	public GameObject m_objProgressBg		= null;

	public Image m_imgIcon					= null;
	public Image m_imgProgressBar			= null;


	public void Initialize(ZAchievementsData p_data)
	{
		m_achievementID = p_data.AchievementID;
		m_textAchievementTitle.text = p_data.AchievementTitle;
		m_textAchievementDesc.text = p_data.AchievementDescription;
		m_textProgress.text = "" + Mathf.Round (p_data.CurrentProgress) + "/" + Mathf.Round (p_data.ProgressToComplete);

		if (!p_data.IsCompleted) 
		{
			m_imgIcon.sprite = p_data.IconAchievementUnlocked;
			m_imgProgressBar.fillAmount = remapValue (p_data.CurrentProgress, 0, p_data.ProgressToComplete, 0, 1);
			m_imgProgressBar.gameObject.SetActive (true);
			m_objProgressBg.gameObject.SetActive (true);

			m_objClaimBtn.gameObject.SetActive (false);
		} 
		else 
		{
			m_imgIcon.sprite = p_data.IconAchievementLocked;
			m_imgProgressBar.gameObject.SetActive (false);
			m_objProgressBg.gameObject.SetActive (false);

			if (p_data.IsClaimed) 
			{
				m_objClaimBtn.gameObject.SetActive (false);
			} 
			else 
			{
				m_objClaimBtn.gameObject.SetActive (true);
			}
		}
	}

	public void ClaimButton()
	{
		m_objClaimBtn.gameObject.SetActive (false);
		ZAchievementsManager.Instance.ClaimButton (m_achievementID);
	}

	private float remapValue(float p_value, float p_from1, float p_to1, float p_from2, float p_to2)
	{
		return (p_value - p_from1) / (p_to1 - p_from1) * (p_to2 - p_from2) + p_from2;
	}
}
