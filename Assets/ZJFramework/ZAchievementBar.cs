﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum BAR_STATE
{
	IDLE = 0,
	SLIDE_IN,
	DISPLAY,
	SLIDE_OUT
}

public class ZAchievementBar : MonoBehaviour 
{
	public static float MIN_DISTANCE 					= 0.1f;
	public static float SLIDEIN_POS_Y 					= 274.0f;
	public static float SLIDEOUT_POS_Y 					= 374.0f;

	public BAR_STATE m_curState							= BAR_STATE.IDLE;

	public Text m_textAchievementTitle					= null;
	public Text m_textAchievementDesc					= null;

	public Image m_imgAchievementIcon					= null;

	public float m_displayTime							= 0.0f;
	public float m_dropSpeed 							= 200.0f;

	private List<ZAchievementsData> m_listOfAchievements = new List<ZAchievementsData>();
	private int m_achievementIterator;
	private float m_time;

	void Update () 
	{
		switch (m_curState) 
		{
			case BAR_STATE.IDLE:
			{
				QueueChecker ();
				break;
			}
			case BAR_STATE.SLIDE_IN:
			{
				Vector3 targetVec = new Vector3 (this.transform.localPosition.x,
							                    SLIDEIN_POS_Y,
												this.transform.localPosition.z);
				Vector3 currentVec = this.transform.localPosition;

				if (Vector3.Distance(currentVec, targetVec) > MIN_DISTANCE) 
				{
					Vector3 moveTransform = Vector3.MoveTowards (currentVec, targetVec, m_dropSpeed * Time.deltaTime);
					this.GetComponent<RectTransform> ().anchoredPosition3D = moveTransform;				
				} 
				else 
				{
					// Add Audio Queue Here
					m_curState = BAR_STATE.DISPLAY;
				}
				break;
			}

			case BAR_STATE.DISPLAY:
			{
				DisplayTimer ();
				break;
			}

			case BAR_STATE.SLIDE_OUT:
			{
				Vector3 targetVec = new Vector3 (this.transform.localPosition.x,
												SLIDEOUT_POS_Y,
												this.transform.localPosition.z);
				Vector3 currentVec = this.transform.localPosition;

				if (Vector3.Distance(currentVec, targetVec) > MIN_DISTANCE) 
				{
					Vector3 moveTransform = Vector3.MoveTowards (currentVec, targetVec, m_dropSpeed * Time.deltaTime);
					this.GetComponent<RectTransform> ().anchoredPosition3D = moveTransform;
				} 
				else 
				{
					m_achievementIterator ++;

					if (m_achievementIterator == m_listOfAchievements.Count) 
					{
						m_achievementIterator = 0;
						m_listOfAchievements.Clear ();
					}
					m_curState = BAR_STATE.IDLE;
				}
				break;
			}
		}
	}

	public void DisplayAchievement(ZAchievementsData p_data)
	{
		m_listOfAchievements.Add (p_data);
	}

	private void QueueChecker()
	{
		if (m_listOfAchievements.Count <= m_achievementIterator)
			return;

		m_textAchievementTitle.text = m_listOfAchievements[m_achievementIterator].AchievementTitle;
		m_textAchievementDesc.text = m_listOfAchievements[m_achievementIterator].AchievementDescription;
		m_imgAchievementIcon.sprite = m_listOfAchievements[m_achievementIterator].IconAchievementUnlocked;

		m_curState = BAR_STATE.SLIDE_IN;
	}

	private void DisplayTimer()
	{
		if (m_time < m_displayTime) 
		{
			m_time += Time.deltaTime;
		} 
		else 
		{
			m_curState = BAR_STATE.SLIDE_OUT;
			m_time = 0;
		}
	}
}
