﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZAnalyticsManager : ZSingleton<ZAnalyticsManager> 
{
	public void Start()
	{
		Firebase.Analytics.FirebaseAnalytics.LogEvent (Firebase.Analytics.FirebaseAnalytics.EventLogin);
	}

	// IAP EVENTS
	public void SendBusinessEvent(string p_currency, int p_amount, string p_itemType, string p_itemID, string p_cartType)
	{
		Firebase.Analytics.Parameter[] SendBusinessParameters = {
			new Firebase.Analytics.Parameter (
				p_currency, p_amount),
			new Firebase.Analytics.Parameter (
				p_itemType, p_itemID),
			new Firebase.Analytics.Parameter (
				p_cartType, ""),
		};

		Firebase.Analytics.FirebaseAnalytics.LogEvent ("sendbusinessevent", SendBusinessParameters);
	}

	// EARN EVENTS
	// Implement on Shop
	public void SendSpendShopItemEvent(string p_itemId, float p_price)
	{
		Firebase.Analytics.Parameter[] SpendShopParameters = {
															new Firebase.Analytics.Parameter(
																"coin", p_price),
															new Firebase.Analytics.Parameter(
																"shop", p_itemId),
															};

		Firebase.Analytics.FirebaseAnalytics.LogEvent ("spendShopEvent", SpendShopParameters);
	}

	// Implement on all areas with earn events, AddCoin
	public void SendEarnCoinsEvent(string p_itemId, float p_price)
	{
		Firebase.Analytics.Parameter[] EarnCoinsParameters = {
															new Firebase.Analytics.Parameter ("coin", p_price),
															new Firebase.Analytics.Parameter ("shop", p_itemId),
														};
		Firebase.Analytics.FirebaseAnalytics.LogEvent("earnCoinEvent", EarnCoinsParameters);
	}


	// PROGRESS EVENTS
	// Put on Success
	public void SendLevelComplete(string p_modeName, int p_levelNumber, int p_tries)
	{
		Firebase.Analytics.FirebaseAnalytics.LogEvent (p_modeName, "levelcompleted" + p_levelNumber, p_tries);
	}

	// Put on Setup Level
	public void SendLevelStart(string p_modeName, int p_levelNumber, int p_tries)
	{
		Firebase.Analytics.FirebaseAnalytics.LogEvent (p_modeName, "levelstarted" + p_levelNumber, p_tries);
	}

	// Put on Setup Level
	public void SendLevelFail(string p_modeName, int p_levelNumber, int p_currentScore)
	{
		Firebase.Analytics.FirebaseAnalytics.LogEvent (p_modeName, "levelfailed" + p_levelNumber, p_currentScore);
	}

	// Put on Success
	public void SendCharComplete(string p_charType, int p_levelNumber)
	{
		Firebase.Analytics.FirebaseAnalytics.LogEvent ("charcomplete", p_charType + p_levelNumber, 0);
	}

	// REWARD EVENTS
	public void SendLikeUsComplete()
	{
		Firebase.Analytics.FirebaseAnalytics.LogEvent ("rewards", "likeuscomplete", 0);
	}

	public void SendSubscribeComplete()
	{
		Firebase.Analytics.FirebaseAnalytics.LogEvent ("rewards", "subscribe", 0);
	}

	public void SendFollowTwitchComplete()
	{
		Firebase.Analytics.FirebaseAnalytics.LogEvent ("rewards", "followtwitch", 0);
	}

	public void SendFollowUsComplete()
	{
		Firebase.Analytics.FirebaseAnalytics.LogEvent ("rewards", "followuscomplete", 0);
	}

	public void SendRateUsComplete()
	{
		Firebase.Analytics.FirebaseAnalytics.LogEvent ("rewards", "rateuscomplete", 0);
	}

	public void SendShareComplete()
	{
		Firebase.Analytics.FirebaseAnalytics.LogEvent ("rewards", "sharecomplete", 0);
	}

	public void SendMoreGamesComplete()
	{
		Firebase.Analytics.FirebaseAnalytics.LogEvent ("rewards", "moregamescomplete", 0);
	}
		
	public void SendEarnComplete()
	{
		Firebase.Analytics.FirebaseAnalytics.LogEvent ("rewards", "watchcomplete", 0);
	}

	public void SendLeaderboardsComplete()
	{
		Firebase.Analytics.FirebaseAnalytics.LogEvent ("rewards", "leaderboardscomplete", 0);
	}

	public void SendShopComplete()
	{
		Firebase.Analytics.FirebaseAnalytics.LogEvent ("rewards", "shopcomplete", 0);
	}

	public void SendSkipComplete()
	{
		Firebase.Analytics.FirebaseAnalytics.LogEvent("rewards", "skipcomplete", 0);
	}
}
