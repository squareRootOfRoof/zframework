﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZShopItemEntry : MonoBehaviour 
{
	public Text m_textItemName; 
	public Image m_imgItem;

	public void Initialize(ZShopItem p_itemEntry)
	{
		m_textItemName.text = p_itemEntry.ItemName;
		m_imgItem.overrideSprite = p_itemEntry.ItemSprite;
	}
}
