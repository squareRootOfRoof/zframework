﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZOnlineNotificationManager : MonoBehaviour {

	public Text m_textTokenReceuved;
	public Text m_textMessageRecieved;

//	public void Start()
//	{
//		Debug.Log ("ster");
//		Debug.Log("Unity Firebase App started");
//		Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
//		Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
//	}
//
//	public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
//	{
//		Debug.Log("Received Registration Token: " + token.Token);
//	}
//
//	public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
//	{
//		Debug.Log("Received a new message from: " + e.Message.From);
//	}   
	// Use this for initialization
	void Start () 
	{
		Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
		Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
	{
		UnityEngine.Debug.Log("Received Registration Token: " + token.Token);
		m_textTokenReceuved.text = "" + token.Token;
	}

	public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e) {
		UnityEngine.Debug.Log("Received a new message from: " + e.Message.From);
		m_textMessageRecieved.text = "" + e.Message.ToString() + "/" + e.ToString() + "/" + e.Message + "?";
	}
}
