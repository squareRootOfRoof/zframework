﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DemoManager : MonoBehaviour {

	public GameObject m_demoScreen;
	public GameObject m_achievementScreen;
	public GameObject m_shopScreen;

	public InputField m_field;

	void Start () 
	{
		ZAchievementsManager.Instance.onAchievementClaimed += ClaimAchievement;
		ZShopManager.Instance.onBuyitem += BuyButton;
	}
	
	// Update is called once per frame
	void Update () 
	{
//		if (Input.GetKeyDown (KeyCode.Q)) 
//		{
//			ZAchievementsManager.Instance ().SetProgress (1, 1);
//		}
//
//		if (Input.GetKeyDown (KeyCode.Space)) {
//			ZAchievementsManager.Instance ().RefreshScreen ();
//		}
	}
		
	public void ShowShop()
	{
		m_demoScreen.gameObject.SetActive (false);
		m_shopScreen.gameObject.SetActive (true);
		m_shopScreen.transform.position = new Vector3 (10000, 10000, 10000);
		m_achievementScreen.gameObject.SetActive (false);
		Debug.Log ("Show ShopButton");
	}

	public void ShowAchievements()
	{
		m_demoScreen.gameObject.SetActive (false);
		m_shopScreen.gameObject.SetActive (false);
		m_achievementScreen.gameObject.SetActive (true);
		Debug.Log ("Show Achievements");
	}

	public void ShowDemo()
	{
		m_demoScreen.gameObject.SetActive (true);
		m_shopScreen.gameObject.SetActive (false);
		m_achievementScreen.gameObject.SetActive (false);
		Debug.Log ("Close Shop");
	}

	public void AddProgress()
	{
		int res = 0;
		if (int.TryParse (m_field.text, out res)) 
		{
			int achievementId = int.Parse (m_field.text);
			ZAchievementsManager.Instance.SetProgress (achievementId, 1);
		}
	}

	public void ShowCrossPromotion()
	{
        ZCrossPromotionManager.Instance.forceShowCrossPromo();
	}

	public void ShowLocalNotification()
	{
		ZNotificationMgr.Instance.AddNotification ("ZFrameworkNotification", 1);
	}

	public void SendLogs()
	{
		ZAnalyticsManager.Instance.SendSubscribeComplete ();
	}

	private void BuyButton(ZShopItem p_shopItem)
	{
		ZShopManager.Instance.ConfirmPurchase (p_shopItem.ItemID);
		Debug.Log ("ItemBought: " + p_shopItem.ItemName);
	}

	private void ClaimAchievement(int p_id)
	{
		Debug.Log ("Achievement ID: " + p_id + " claimed");
	}
}
